// var SourceNode = require( 'source-map' ).SourceNode;
var SourceMapConsumer = require( 'source-map' ).SourceMapConsumer;
var SourceMapGenerator = require( 'source-map' ).SourceMapGenerator;


// Given a string representing a source map, returns a string representing the
// same source map, but compacted.
function compact(str) {

	/// TODO: do not rely on a static path

	var sourceMapJSON = JSON.parse(str);
	var consumer = new SourceMapConsumer(str);
	var generator = new SourceMapGenerator({
		file: sourceMapJSON.file
	});
	for (var i=0; i<sourceMapJSON.sources.length; i++){
		generator.setSourceContent(sourceMapJSON.sources[i], sourceMapJSON.sourcesContent[i]);
	}

	var prev = {
		s: undefined,	// Source
		ol: 0,	// Original Line
		oc: 0,	// Original Column
		gl: 0,	// Generated Line
		gc: 0,	// Generated Column
	}

	var valid = [];

	consumer.eachMapping(function(m){

		var curr = {
			s: m.source,
			ol: m.originalLine,
			oc: m.originalColumn,
			gl: m.generatedLine,
			gc: m.generatedColumn
		};

		// Fill up empty lines with synthetic blank mappings
		while(curr.gl > prev.gl + 1) {
			valid.push(prev);
// 			console.log('Inserting blank line mapping');
			prev = {
				s: prev.s,
				ol: prev.ol + 1,
				oc: 0,
				gl: prev.gl + 1,
				gc: 0
			}
		}

		// Fill up start of the line with a synthetic blank mapping
		if (curr.gl > prev.gl && curr.gl > 0) {
			valid.push(prev);
// 			console.log('Inserting blank line start mapping');
			prev = {
				s: curr.s,
				ol: curr.ol,
				oc: 0,
				gl: curr.gl,
				gc: 0
			}
		}

		// If destination is the same, skip previous
		if (curr.gl === prev.gl && curr.gc === prev.gc) {
// 			console.log('Collapsed original, skipping last mapping');
			prev = curr;

		// If in the same original & generated line, check length of previous segment
		} else if (curr.ol === prev.ol &&
				curr.gl === prev.gl &&
				curr.oc-prev.oc === curr.gc-prev.gc ) {

// 			console.log('Same length segment, skipping current mapping');

		} else {
// 			console.log(prev.s + ': ' + prev.ol + ',' + prev.oc + '→' + prev.gl + ',' + prev.gc );
			valid.push(prev);
			prev = curr;
		}

	}, SourceMapConsumer.GENERATED_ORDER);

// 	console.log(prev.s + ': '+ prev.ol + ',' + prev.oc + '→' + prev.gl + ',' + prev.gc );

	valid.push(prev);


	valid.shift();	// Remove first 'undefined' mapping

	for (var i=0; i<valid.length; i++) {
		var m = valid[i];
		if (m.s !== undefined) {
			generator.addMapping({
				source: m.s,
				original: { line: m.ol, column: m.oc },
				generated: { line: m.gl, column: m.gc }
			});
		}
	}

// 	console.log(valid);

	return generator.toString();
}





module.exports = compact;
