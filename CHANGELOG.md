# 1.0.1

* Do not throw error when first lines are synthetic (UMD banner, etc)

# 1.0.0

* Initial release
